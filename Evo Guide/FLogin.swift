 //
//  FLogin.swift
//  FruitfulLibraries
//
//  Created by Gaurav Ravindra on 01/05/2015.
//  Copyright (c) 2015 Fruitful Business Services. All rights reserved.
//

import Foundation
import UIKit


public class FLoginControl {
    
    private static var _instance:(FLoginControl) = FLoginControl()
    
    private var request:JSONUrlRequest = JSONUrlRequest()
    var OnLoginSuccess:(() -> ())?
    var OnLoginFail:((text: String) -> ())?
    var OnTokenValid:(() -> ())?
    var OnTokenInvalid:(() -> ())?
    var OnLogout:(() -> ())? 
    
    var _userName:(String) = ""
    var _clientId:(String) = "6"
    var _token:(String) = ""
    var _isInit:(Bool) = false
    
    
    
    init(){
        
    }
    
    
    class func UserName() -> String { return _instance._userName }
    class func Token() -> String { return _instance._token }
    class func ClientID() -> String { return _instance._clientId }
    class func IsInit() -> Bool { return _instance._isInit }
    
    
    class func SetOnLoginSuccess(function: () -> () ) {  _instance.OnLoginSuccess = function }
    class func SetOnLoginFail(function: (text:String) -> ()) { _instance.OnLoginFail = function }
    class func SetOnTokenValid(function: () -> () ) {  _instance.OnTokenValid = function }
    class func SetOnTokenInvalid(function: () -> ()) { _instance.OnTokenInvalid = function }
    class func SetOnLogout(function: () -> () ) {  _instance.OnLogout = function }
    
    
    class func InitController() -> Bool{
        
        //Get existing user login data.
        _instance = FLoginControl()
        _instance.LoadUserPrefs()
        
        _instance._isInit = true
        return _instance._isInit
    }
    
    
    
    class func Login(User user:String, Password pass:String, ShowAlert sAlert:Bool = false){
    
        //If already logged in then don't login again.
        if(self.IsLoggedIn()) {
            dispatch_async(dispatch_get_main_queue(), {
                if let _ = self._instance.OnLoginSuccess { self._instance.OnLoginSuccess!() }
            });
            return
        }
        
        self.Logout()
        _instance._userName = user
        _instance._token = ""
        //_instance._clientId = ""
        
        
        _instance.request  = JSONUrlRequest()
        _instance.request.SetURL(UrlString: Fruitful.API_URL_LOGIN)
        
        _instance.request.AddParameter(Name: "Username", Value: user.stringByReplacingOccurrencesOfString("\n", withString: "", options: [], range: nil))
        _instance.request.AddParameter(Name: "Password", Value: pass.stringByReplacingOccurrencesOfString("\n", withString: "", options: [], range: nil))
        
        _instance.request.OnRequestSuccess = _instance._loginSucess
        _instance.request.OnRequestFail = _instance._loginFail
        
        _instance.request.GetSingleObjectFromUrlRequest(ShowAlert: sAlert)
        
    }
    
    private func _loginSucess(text:String){
        
        if let dic:NSDictionary = request.ParsedObjects[0] as? NSDictionary {
            
            if let t:String = dic.objectForKey("token") as? String, let c:String = dic.objectForKey("clientid") as? String {
                
                self._token = t
                self._clientId = c
                
                SaveUserPrefs()
                dispatch_async(dispatch_get_main_queue(), {
                    if let _ = self.OnLoginSuccess { self.OnLoginSuccess!() }
                });
                return
                
            }
            else {
                _loginFail("Failed to get login credentials")
            }
        }
        else{
            _loginFail("Failed to get login credentials")
        }
        
    }
    
    private func _loginFail(t:String){
        
        #if DEBUG
            print("Login Failed: \(t)")
        #endif
        
        
        SaveUserPrefs()
        FLoginControl.Logout()
        dispatch_async(dispatch_get_main_queue(), {
            if let _ = self.OnLoginFail { self.OnLoginFail!(text: t) }
        });
        
    }
    
    

    
    class func IsLoggedIn()->Bool {
        if(!_instance._userName.isEmpty && !_instance._token.isEmpty) {
            return true
        }
        else {
            return false
        }
    }
    
    class func CheckToken() {
        _instance.request  = JSONUrlRequest()
        _instance.request.SetURL(UrlString: Fruitful.API_URL_CHECK_TOKEN)
        
        _instance.request.AddParameter(Name: "Username", Value: _instance._userName.stringByReplacingOccurrencesOfString("\n", withString: "", options: [], range: nil))
        _instance.request.AddParameter(Name: "Token", Value: _instance._token.stringByReplacingOccurrencesOfString("\n", withString: "", options: [], range: nil))
        
        _instance.request.OnRequestSuccess = _instance._tokenValid
        _instance.request.GetStringFromUrlRequest(ShowAlert: false)
        
    }
    
    private func _tokenValid(text:String) {
        if(text == "Success") {
            dispatch_async(dispatch_get_main_queue(), {
                if let _ = self.OnTokenValid { self.OnTokenValid!() }
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), {
                if let _ = self.OnTokenInvalid { self.OnTokenInvalid!() }
            });
        }
    }
    
    
    
    class func Logout()->Bool {
        
        //Clear all login info (except "User Name@ because we can use that later as a user pref
        //userName = ""
        _instance._token = ""
        //_instance._clientId = ""
         _instance.SaveUserPrefs()

        return true;
    }

    private func SaveUserPrefs() {
        let standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        standardUserDefaults.setObject(_userName, forKey: "userName")
        standardUserDefaults.setObject(_token, forKey: "token")
        //standardUserDefaults.setObject(_clientId, forKey: "clientId")
        
        NSUserDefaults.standardUserDefaults().synchronize()
        
    }
    
    
    private func LoadUserPrefs() {
        let standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        if let y = standardUserDefaults.stringForKey("userName") { _userName = y } else { _userName = "" }
        if let y = standardUserDefaults.stringForKey("token") { _token = y } else { _token = "" }
        //if let y = standardUserDefaults.stringForKey("clientId") { _clientId = y } else { _clientId = "" }
                
    }
    
    
    
}