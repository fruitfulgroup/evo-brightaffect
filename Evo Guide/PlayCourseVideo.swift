//
//  PlayCourseVideo.swift
//  Bright Affect
//
//  Created by Zee on 28/05/2015.
//  Copyright (c) 2015 Bright Affect. All rights reserved.
//

import UIKit
import CoreData
import AVKit
import AVFoundation

class PlayCourseVideo: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    var course:NSManagedObject!
    var photo:UIImage!
    var videos:[NSManagedObject]!
    var seenVideos: Array<Bool> = []
    var saveOffline = NSMutableDictionary()
    var courseID: Int = 0
    
    @IBOutlet var b_playButton: UIButton!
    @IBOutlet var iv_coursePhoto: UIImageView!
    @IBOutlet var l_title: UILabel!
    @IBOutlet var tv_script: UITextView!
    @IBOutlet var tableview: UITableView!
    @IBOutlet var v_VideoView: UIView!
    @IBOutlet var sw_Offline: UISwitch!
    
    var moviePlayer:AVPlayer = AVPlayer()
    let movieController:AVPlayerViewController = AVPlayerViewController()
    var downloadingAlert: UIAlertController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set the client logo image as the navigation bar title
        let imgView = UIImageView(image: UIImage(named: "evo logo long.png"))
        imgView.contentMode = .ScaleAspectFit
        imgView.frame = CGRect(x: imgView.frame.origin.x, y: imgView.frame.origin.y, width: imgView.frame.width, height: 20)
        
        self.navigationItem.titleView = imgView
        self.navigationItem.backBarButtonItem?.title = "  "
        
        self.tableview.delegate = self
        self.tableview.dataSource = self
        
        self.tableview.selectRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0) , animated: false, scrollPosition: UITableViewScrollPosition.None)
        for _ in videos {  seenVideos.append(false) }
        
        if let y = course.valueForKey("title") as? String { self.navigationItem.title = y }
        
        if let p = photo { iv_coursePhoto.image = p }
        
        courseID = (course.valueForKey("course_id") as? Int)!
        
        if let download:Bool = self.saveOffline.valueForKey("\(courseID)") as? Bool {
            sw_Offline.on = download
        }
        
        sw_Offline.addTarget(self, action: Selector("stateChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        
        
        downloadingAlert = UIAlertController(title: FLocal.LocalizedString("DOWNLOADING", comment: ""), message: nil, preferredStyle: .Alert)
        
        
        //Get video from index
        if videos.count > 0 {
            
            //Set Text information
            let obj = videos[0]
            if let y:String = obj.valueForKey("title") as? String { l_title.text = y }
            if let y:String = obj.valueForKey("script") as? String { tv_script.text = y }
        }
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        
        if self.isMovingFromParentViewController() || self.isBeingDismissed() {
            moviePlayer.pause()
        }
    }
    
    
    @IBAction func Play(sender: UIButton) {
        
        self.iv_coursePhoto.hidden = true
        self.b_playButton.hidden = true
        
        PlaySelectedVideo()
    }
    
    func PlaySelectedVideo() {
        let index = self.tableview.indexPathForSelectedRow
        seenVideos[index!.row] = true
        moviePlayer.pause()
        
        moviePlayer.replaceCurrentItemWithPlayerItem(nil)
        
        self.tableview.reloadData()
        self.tableview.selectRowAtIndexPath(index, animated: true, scrollPosition: UITableViewScrollPosition.None)

        //Get video from selected index of the table
        let obj = videos[index!.row]
        
        //Set Text information
        if let y:String = obj.valueForKey("title") as? String { l_title.text = y }
        if let y:String = obj.valueForKey("script") as? String { tv_script.text = y }
        
        
        if let vid:String = obj.valueForKey("video") as? String {
            
            //If the file is on the device load from storage else stream using saved link
            if Fruitful.FileExists(Filename: vid, Path: "/Videos/") {
                let url = NSURL(fileURLWithPath: "\(Fruitful.GetFilePath())/Videos/\(vid)" )
                moviePlayer = AVPlayer(URL: url)
            }
            else {
                
                if let link = obj.valueForKey("video_link") as? String {
                    let link2 = link.stringByRemovingPercentEncoding!
                    
                    if let link_encoded = link2.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) {
                        if let url = NSURL(string: link_encoded) {
                            if url.checkResourceIsReachableAndReturnError(nil) == false {
                                moviePlayer = AVPlayer(URL: url)
                            }
                        }
                    }
                }
                else {
                    return
                }
                
            }
            
            movieController.player = moviePlayer
            self.addChildViewController(movieController)
            movieController.view.frame = v_VideoView.frame
            movieController.view.sizeToFit()
            movieController.videoGravity = AVLayerVideoGravityResizeAspect
            moviePlayer.play()
            v_VideoView.addSubview( movieController.view )
            
        }
        
    }
    
    
    func stateChanged(switchState: UISwitch) {
        
        //Download/Delete videos depending on the change made to the switch
        if switchState.on {
            let alert = UIAlertController(title: FLocal.LocalizedString("SAVE_OFFLINE", comment: ""), message: FLocal.LocalizedString("SAVE_OFFLINE_TEXT", comment: ""), preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: FLocal.LocalizedString("NO", comment: ""), style: UIAlertActionStyle.Default, handler: { alertAction in
                
                switchState.on = false
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: FLocal.LocalizedString("YES", comment: ""), style: UIAlertActionStyle.Default, handler: { alertAction in
                self.saveOffline.setValue(true, forKey: "\(self.courseID)")
                self.DownloadVideos()
                alert.dismissViewControllerAnimated(true, completion: nil)
                
            }))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: FLocal.LocalizedString("REMOVE_OFFLINE", comment: ""), message: FLocal.LocalizedString("REMOVE_OFFLINE_TEXT", comment: ""), preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: FLocal.LocalizedString("NO", comment: ""), style: UIAlertActionStyle.Default, handler: { alertAction in
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: FLocal.LocalizedString("YES", comment: ""), style: UIAlertActionStyle.Default, handler: { alertAction in
                self.saveOffline.setValue(false, forKey: "\(self.courseID)")
                let standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
                standardUserDefaults.setObject(self.saveOffline, forKey: "offline")
                NSUserDefaults.standardUserDefaults().synchronize()
                
                //Delete all videos
                for vid in self.videos {
                    let videoName = vid.valueForKey("video") as! String
                    Fruitful.DeleteFile(Filename: videoName, Path: "/Videos/")
                }
                
                switchState.on = false
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    
    func DownloadVideos() {
        
        let priority = DISPATCH_QUEUE_PRIORITY_LOW
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            
            dispatch_async(dispatch_get_main_queue(), {
                self.presentViewController(self.downloadingAlert, animated: true, completion: nil)
            })
            
            var current = 1
            
            for vid in self.videos {
                dispatch_async(dispatch_get_main_queue(), {
                    self.downloadingAlert.title = FLocal.LocalizedString("DOWNLOADING", comment: "")  + " \(current) / \(self.videos.count)"
                    current++
                })
                
                let videoName = vid.valueForKey("video") as! String
                
                if let videolink = vid.valueForKey("video_link") as? String {
                    let videolink2 = videolink.stringByRemovingPercentEncoding!
                    if let encoded_link = videolink2.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) {
                        
                        
                        if !Fruitful.FileExists(Filename: videoName, Path: "/Videos/"){
                            
                            if let url = NSURL(string: encoded_link ) {
                                if let data:NSData = NSData(contentsOfURL: url){
                                    Fruitful.SaveFile(data, Filename: videoName, Path: "/Videos/")
                                    print("Downloaded: \(videoName)")
                                }
                            }
                        }
                    }
                }
                
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                self.downloadingAlert.dismissViewControllerAnimated(true, completion: nil)
                let standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
                standardUserDefaults.setObject(self.saveOffline, forKey: "offline")
                NSUserDefaults.standardUserDefaults().synchronize()
                
            })
            
        }
    }
    
    
    
    
    
    
    
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return videos.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("VideoCell", forIndexPath: indexPath)
        
        let vid = videos[indexPath.row]
        
        if let y = vid.valueForKey("title") as? String { cell.textLabel!.text = y }
        
        cell.accessoryType = seenVideos[indexPath.row]  ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if iv_coursePhoto.hidden {
            PlaySelectedVideo()
        }
    }


    
}
