//
//  FLocal.swift
//  Bright Affect
//
//  Created by Zee on 08/06/2015.
//  Copyright (c) 2015 Bright Affect. All rights reserved.
//

//This class if responisable for the Localization of the app.

import Foundation
import UIKit


public class FLocal {
    
    private static let _instance:FLocal = FLocal()
    private var langBundle:NSBundle = NSBundle.mainBundle()
    
    
    
    class func InitLocalWithCode(Code code:String) {
        
        if let _ = UIApplication.sharedApplication().delegate {
            if let path = NSBundle.mainBundle().pathForResource(code, ofType: "lproj") {            
                if let b = NSBundle(path: path) {
                    _instance.langBundle = b
                }
            }
        }
        
    }
    
    //Get localized string based on the language set in UserPrefs.
    class func LocalizedString(key:String, comment c: String = "") -> String{
        
        return _instance.langBundle.localizedStringForKey(key, value: key, table: nil)
    }
    
    
}

/*
    UI Extensions
    -----------------------
    This UI extensions are add to the ui elements in the storyboard editor.
    This automatically localize the ui text in the input reference string.

*/

extension UILabel {
    
    var localizedText: String {
        set (key) { text = FLocal.LocalizedString(key)  }
        get { return text!  }
    }
    
}

extension UITextField {
    
    var localizedText: String {
        set (key) { text = FLocal.LocalizedString(key, comment: "")  }
        get { return text!  }
    }
    
    var localizedPlaceholder: String {
        set (key) { placeholder = FLocal.LocalizedString(key, comment: "")  }
        get { return placeholder!  }
    }
}


extension UIButton {
    var localizedTitle: String {
        set (key) {
            setTitle(FLocal.LocalizedString(key, comment: ""), forState: .Normal)
            setTitle(FLocal.LocalizedString(key, comment: ""), forState: .Highlighted)
            setTitle(FLocal.LocalizedString(key, comment: ""), forState: .Disabled)
        }
        get { return titleForState(.Normal)! }
    }
}