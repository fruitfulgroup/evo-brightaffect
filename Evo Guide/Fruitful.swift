//
//  Fruitful.swift
//  FruitfulLibraries
//
//  Created by Gaurav Ravindra on 01/05/2015.
//  Copyright (c) 2015 Fruitful Business Services. All rights reserved.
//

import Foundation
import UIKit

public class Fruitful {
    
    public static let URL:(String) = "http://brightaffect.fruitfulserver.com/"
    public static let API_URL:(String) = "http://brightaffect.fruitfulserver.com/API/"
    public static let API_URL_LOGIN:(String) = "http:/brightaffect.fruitfulserver.com/API/Login"
    public static let API_URL_CHECK_TOKEN:(String) = "http://brightaffect.fruitfulserver.com/API/IsTokenValid"
    public static let API_URL_GET_COMPANY_DETAILS:(String) = "http://brightaffect.fruitfulserver.com/API/CompanyDetails"
    
    public static var LANGUAGE_ID = 0
    
    public static let API_BOUNDARY:(String) = "--StopStiffingOurPostDataYouBanana"
    
    
    class func DateFromJSON(dateString: String) -> NSDate?{
        
        var newString = dateString.stringByReplacingOccurrencesOfString("/Date(", withString: "", options: [], range: nil)
        newString = newString.stringByReplacingOccurrencesOfString(")", withString: "", options: [], range: nil)
        
        let milliseconds:Int64 = (newString as NSString).longLongValue
        let interval:NSTimeInterval = NSNumber(longLong: milliseconds/1000).doubleValue
        
        return NSDate(timeIntervalSince1970: interval)
    }
    
    class func DateFromString(date:String,  format fmt:String = "HH:mm dd/MM/yy") -> NSDate?{
        
        let dateF:NSDateFormatter = NSDateFormatter()
        dateF.dateFormat = fmt
        
        if let y = dateF.dateFromString(date) { return y }
        else { return nil }
        
    }
    
    
    class func StringFromDate(date:NSDate,  format fmt:String = "HH:mm dd/MM/yy") -> String?{
        
        let dateF:NSDateFormatter = NSDateFormatter()
        dateF.dateFormat = fmt
        
        return dateF.stringFromDate(date)
    }
    
    
    class func StringFromDouble(val:Double, DecimalPoints pts: Int = 2) -> String?{
        
        let numF:NSNumberFormatter = NSNumberFormatter()
        numF.numberStyle = NSNumberFormatterStyle.DecimalStyle
        numF.minimumFractionDigits = pts
        numF.maximumFractionDigits = pts
        
        if let y = numF.stringFromNumber(val) {  return y }
        else { "" as String }
        
        return ""
    }
    
    
    class func SaveFile(file:NSData, Filename name:String, Path path:String = "/") -> Bool{
        
        let filePath = GetFilePath() + path
        
        if (!NSFileManager.defaultManager().fileExistsAtPath(filePath)) {
            do {
                try NSFileManager.defaultManager().createDirectoryAtPath(filePath, withIntermediateDirectories: false, attributes: nil)
            } catch _ {
            }
        }
        
        let success = file.writeToFile(filePath + name, atomically: true)
        return success
        
    }
    
    class func LoadFile(Filename name:String, Path path:String = "/") -> NSData{
        
        let filePath = GetFilePath() + path + name
        
        if let d = NSData(contentsOfFile: filePath) {
            return d
        }
        else {
            return NSData()
        }
        
    }
    
    
    class func DeleteFile(Filename name:String, Path path:String = "/") -> Bool{
        
        let filePath = GetFilePath() + path + name
        
        do {
            try NSFileManager.defaultManager().removeItemAtPath(filePath)
            return true
        } catch _ {
            return false
        }
        
    }
    
    
    class func FileExists(Filename name:String, Path path:String = "/") -> Bool{
        
        let filePath = GetFilePath() + path + name
        
        return NSFileManager.defaultManager().fileExistsAtPath(filePath)
        
    }
    
    class func FileCreatedDate(Filename name:String, Path path:String = "/") -> NSDate {
        
        let filePath = GetFilePath() + path + name
        
        if NSFileManager.defaultManager().fileExistsAtPath(filePath) {
            
            if let attributes = try? NSFileManager.defaultManager().attributesOfItemAtPath(filePath){
                if let creationDate = attributes["NSFileCreationDate"] as? NSDate {
                    return creationDate
                }
            }
            
        }
        else {
            
        }
        
        return NSDate()
        
    }
    
    
    class func ListOfFiles(Path path:String = "/") -> Array<String>{
        
        let filePath = GetFilePath() + path
        
        do {
            let y = try NSFileManager.defaultManager().contentsOfDirectoryAtPath(filePath)
            return y
        }
        catch _ {
            return []
        }
        
    }
    
    
    class func DeleteAllFiles(Path path:String = "/"){
        
        let filePath = GetFilePath() + path
        
        do {
            let files = try NSFileManager.defaultManager().contentsOfDirectoryAtPath(filePath)
            
            for f:String in files {
                do {
                    try NSFileManager.defaultManager().removeItemAtPath(filePath + f)
                } catch _ {
                }
            }
            
        }
        catch _ { }
        
        
    }
    
    class func GetFilePath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask , true)
        
        if paths.count > 0 {
            return paths[0]
        }
        else {
            return ""
        }
        
    }
    
    
    
}





