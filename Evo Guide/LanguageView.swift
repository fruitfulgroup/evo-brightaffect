//
//  LanguageView.swift
//  Bright Affect
//
//  Created by Zee on 20/05/2015.
//  Copyright (c) 2015 Bright Affect. All rights reserved.
//

import UIKit
import CoreData


//This is the Language select view.
//This is where the user selects the language settings of the app. This avoids using the default language of the device.

class LanguageView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewDidLoad()
        
        
        //Check to see if the language has already been set. Go to the Login screen.
        let standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let y = standardUserDefaults.objectForKey("LangID") as? Int
        
        if y != nil {
            GoToLogin(y!, animated: false)
        }

    }
    
       
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func LanguageSelected(sender: UIButton) {
        
        let standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if let y = standardUserDefaults.objectForKey("PreLangID")?.integerValue {
            
            if y != sender.tag {
                Fruitful.DeleteAllFiles(Path: "/Videos/")
                Fruitful.DeleteAllFiles(Path: "/Photos/")
                
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                if let managedContext:NSManagedObjectContext = appDelegate.managedObjectContext {
                
                    //Clear/Delete old video data first

                    if let oldCourses = (try? managedContext.executeFetchRequest(NSFetchRequest(entityName: "Courses"))) as? [NSManagedObject] {
                        for course:NSManagedObject in oldCourses {
                            managedContext.deleteObject(course)
                        }
                    }
                
                    if let oldVids = (try? managedContext.executeFetchRequest(NSFetchRequest(entityName: "Videos"))) as? [NSManagedObject] {
                        for vid:NSManagedObject in oldVids {
                            managedContext.deleteObject(vid)
                        }
                    }
                
                    do {
                        try managedContext.save()
                    } catch _ {
                    }
                    
                }

            }
        }
        
        GoToLogin(sender.tag, animated: true)

    }
    
    private func GoToLogin(LangID: Int, animated: Bool) {
        //This button function get Language ID and sets it as a user default.
        let standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        Fruitful.LANGUAGE_ID = LangID
        
        switch(LangID) {
            //Set the Localisation class with the correct country code.
            case 1: FLocal.InitLocalWithCode(Code: "en"); break
            case 2: FLocal.InitLocalWithCode(Code: "fr"); break
            case 3: FLocal.InitLocalWithCode(Code: "es"); break
            case 5: FLocal.InitLocalWithCode(Code: "bg"); break
            case 6: FLocal.InitLocalWithCode(Code: "el"); break
            case 7: FLocal.InitLocalWithCode(Code: "ko"); break
            case 8: FLocal.InitLocalWithCode(Code: "it"); break
            case 9: FLocal.InitLocalWithCode(Code: "de"); break
            default: FLocal.InitLocalWithCode(Code: ""); break
        }
        
        
        standardUserDefaults.setObject(LangID, forKey: "LangID");
        standardUserDefaults.setObject(LangID, forKey: "PreLangID");
        NSUserDefaults.standardUserDefaults().synchronize()
        
        //Go directly to the main menu
        if let view = self.storyboard?.instantiateViewControllerWithIdentifier("MainMenu") as? MainMenuView {
            self.navigationController?.pushViewController(view, animated: animated)
        }
        
        /*
        
        NOTE: login screen but has been disabled
        
        if let view = self.storyboard?.instantiateViewControllerWithIdentifier("LoginView") as? LoginView {
            self.navigationController?.pushViewController(view, animated: animated)
        }
        */

    }
    
}
